import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class Calculator {

    static List<Map<String, ArrayList<Integer>>> allExpressoins = new ArrayList<>();

    public static void main(String[] args) {
        File expressionsList = new File("src/main/resources/list.txt");

        allExpressoins = readFromFile(expressionsList);

        for (int i = 0; i< allExpressoins.size(); i++) {
            for (Map.Entry<String, ArrayList<Integer>> entry : allExpressoins.get(i).entrySet()) {
                MathOperator sign = MathOperator.valueOf(entry.getKey().toString());
                ArrayList<Integer> operands =  entry.getValue();  // Преобразование обьекта в коллекцию
                calculate(sign, operands);
            }
        }
    }

    private static List<Map<String, ArrayList<Integer>>> readFromFile(File expressionsList) {
        List<Map<String, ArrayList<Integer>>> allExp = new LinkedList<>();
        List<String> lines = null;
        try {
            lines = Files.readAllLines(expressionsList.toPath());
        }catch (IOException e) {
            e.printStackTrace();
        }
        for (String expression: lines) {
            ArrayList<Integer> operands = new ArrayList<>();
            Map<String, ArrayList<Integer>> mathExpression = new HashMap<>();
            String[] split = expression.split(" ");
            operands.add(Integer.parseInt(split[0]));
            operands.add(Integer.parseInt(split[2]));
            mathExpression.put(split[1], operands);
            allExp.add(mathExpression);
        }
        return allExp;
    }

    private enum MathOperator {
            PLUS(" + "),
            MINUS(" - "),
            MULT(" * "),
            DIV(" / "),
            REM(" % ");

            private String symbol;

            MathOperator(String symbol){
                this.symbol = symbol;
            }
            public String getSymbol() {
                return symbol;
            }
        }

    public static void calculate(MathOperator operator, ArrayList<Integer> input) {
        double result=0; // для универсальности под все операции результат типа дабл

        switch(operator) {
            case PLUS:
               result = input.get(0) + input.get(1); // перевод в дабл чтобы не было переполнения
                break;
            case MINUS:
                result = (double) (input.get(0))-(input.get(1)); // перевод в дабл чтобы не было переполнения
                break;
            case MULT:
                result = input.get(0)*input.get(1); // перевод в дабл чтобы не было переполнения
                break;
            case DIV:
               result = (input.get(1) != 0 ) ? ((double) input.get(0) / input.get(1)) : -1;
               // тернарный оператор в случае деления на 0 возвращает -1
               break;
            case REM:
                result = (input.get(1) != 0 ) ? ((double) input.get(0) % input.get(1)) : -1;
                break;
        }
        System.out.println(input.get(0) + operator.getSymbol() + input.get(1) + " = " + result); // вывод результата;
    }
}
