import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class PeopleDB {


    public static void main(String[] args) {
        List<Person> db = new ArrayList<>();
        db = readFromFile(db); // метод добавляет значения в лист

        System.out.println(db);
        choice choice = PeopleDB.choice.KEY;
        searchByID(Integer.parseInt(readInput(choice.KEY)), db);
        searchByValue(readInput(choice.VALUE), db);
    }

    public static List<Person> readFromFile(List<Person> lispPerson) {
        File fileDB = new File("src/main/resources/db.txt");
        List<String> lines = null;

        try {
            lines = Files.readAllLines(fileDB.toPath());
        }catch (IOException e) {
            e.printStackTrace();
        }
        for (String line:lines) {
            String[] split =line.split(" ");
            lispPerson.add(new Person(Integer.parseInt(split[0]), split[1], split[2]));
        }
        return lispPerson;
    }

    public static String readInput(choice ch) {
        Scanner inputScanner = new Scanner(System.in);
        switch (ch.toString()) {
            case "KEY":
                System.out.print("Enter ID: ");

                if (inputScanner.hasNextInt()) {
                    String input = inputScanner.next();
                    return input;
                } else {
                    System.out.println("Wrong ID!!!");
                    return "0";
                }
            case "VALUE":
                System.out.print("Input first or last name: ");
                String input = inputScanner.next();
                inputScanner.close();
                return input;
            default: return "Smth go wrong";
        }
    }

    public static void searchByValue(String value, List<Person> db) {
        boolean notfound = true;
        for (int i = 0; i<db.size(); i++) {
            Person person = db.get(i);
            if ((person.getFirstName().equalsIgnoreCase(value)) ||
                (person.getLastName().equalsIgnoreCase(value))) {
                System.out.println(person.toString());
                notfound = false;
            }
        }
        if (notfound) {
            System.out.println("Not found");
        }
    }

    public static void searchByID(int key, List<Person> db) {
        boolean notfound = true;
        for (int i = 0; i<db.size(); i++) {
            Person person = (Person) db.get(i);
            if (person.getId() == key) {
                System.out.println(person.toString());
                notfound = false;
            }
        }
        if (notfound) {
            System.out.println("ID not found");
        }
    }

    public static class Person{
        private String firstName, lastName;
        private Integer id;

        public Person(Integer id, String firstName, String lastName) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }
        public Integer getId() {
            return id;
        }
        @Override
        public String toString() {
            return id +  "\t" + firstName + "\t" + lastName;
        }
    }
    enum choice {
        KEY,
        VALUE;

    }

}
